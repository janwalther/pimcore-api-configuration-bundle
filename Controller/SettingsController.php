<?php declare(strict_types=1);
/**
 * Implemented by moreperform GmbH team https://www.moreperform.de/Agentur/Team
 *
 * @copyright moreperform GmbH http://moreperform.de
 * @license proprietär
 * @link http://moreperform.de
 */

namespace Mope\ConfigurationBundle\Controller;

use Pimcore\Bundle\AdminBundle\Controller\AdminController;
use Pimcore\Bundle\AdminBundle\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class SettingsController
 * Manage backend settings
 *
 * @package Mope\ConfigurationBundle\Controller
 */
class SettingsController extends AdminController
{
    /**
     * Save settings
     *
     * @Route("/saveswsettings", name="mope_save_config")
     */
    public function saveAction(Request $request)
    {
        if (!$this->getAdminUser()) {
            throw new AccessDeniedHttpException('Only admin user can change configuration');
        }

        $apiUrl = $request->request->get("shopware_api_url");
        $apiUser = $request->request->get("shopware_api_user");
        $apiToken = $request->request->get("shopware_api_token");
        $syncStock = $request->request->get("shopware_api_sync_stock");

        $setting = WebsiteSetting::getByName("shopware_api_url");
        if (!$setting) {
            $setting = new WebsiteSetting();
        }
        $setting->setValues(["name" => "shopware_api_url", "data" => $apiUrl, "type" => "text"]);
        $setting->save();

        $setting = WebsiteSetting::getByName("shopware_api_user");
        if (!$setting) {
            $setting = new WebsiteSetting();
        }
        $setting->setValues(["name" => "shopware_api_user", "data" => $apiUser, "type" => "text"]);
        $setting->save();

        $setting = WebsiteSetting::getByName("shopware_api_token");
        if (!$setting) {
            $setting = new WebsiteSetting();
        }
        $setting->setValues(["name" => "shopware_api_token", "data" => $apiToken, "type" => "text"]);
        $setting->save();

        $setting = WebsiteSetting::getByName("shopware_api_sync_stock");
        if (!$setting) {
            $setting = new WebsiteSetting();
        }
        $setting->setValues(["name" => "shopware_api_sync_stock", "data" => $syncStock, "type" => "text"]);
        $setting->save();

        $data = ["success" => true, "data" => [$apiUrl, $apiUser, $apiToken, $syncStock]];

        return new JsonResponse($data);
    }

    /**
     * Gets settings
     *
     * @Route("/admin/getswsettings", name="mope_get_config")
     */
    public function getAction(Request $request)
    {
        if (!$this->getAdminUser()) {
            throw new AccessDeniedHttpException('Only admin user can change configuration');
        }
        $settingUrl = WebsiteSetting::getByName("shopware_api_url");
        $settingUser = WebsiteSetting::getByName("shopware_api_user");
        $settingToken = WebsiteSetting::getByName("shopware_api_token");
        $settingStock = WebsiteSetting::getByName("shopware_api_sync_stock");
        $settingUrlData = $settingUserData = $settingTokenData = $settingStockData = "";
        if ($settingUrl) {
            $settingUrlData = $settingUrl->getData();
        }
        if ($settingUser) {
            $settingUserData = $settingUser->getData();
        }
        if ($settingToken) {
            $settingTokenData = $settingToken->getData();
        }
        if ($settingStock) {
            $settingStockData = $settingStock->getData();
        }
        $data = ["success" => true,
            "data" => [
                "shopware_api_url" => $settingUrlData,
                "shopware_api_user" => $settingUserData,
                "shopware_api_token" => $settingTokenData,
                'shopware_api_sync_stock' => $settingStockData
            ]
        ];

        return new JsonResponse($data);
    }
}
