<?php declare(strict_types=1);
/**
 * Implemented by moreperform GmbH team https://www.moreperform.de/Agentur/Team
 *
 * @copyright moreperform GmbH http://moreperform.de
 * @license proprietär
 * @link http://moreperform.de
 */

namespace Mope\ConfigurationBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;
use Pimcore\Extension\Bundle\Traits\PackageVersionTrait;

/**
 * Class MopeConfigurationBundle
 * @package Mope\ConfigurationBundle
 */
class MopeConfigurationBundle extends AbstractPimcoreBundle
{
    use PackageVersionTrait;

    /**
     * @return string
     */
    protected function getComposerPackageName(): string
    {
        return 'mope/configurationBundle';
    }

    /**
     * @inheritDoc
     */
    public function getVersion()
    {
        return '1.1.0';
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return 'Provides REST Api configuration for all bundles';
    }

    /**
     * @return array
     */
    public function getJsPaths()
    {
        return [
            '/bundles/mopeconfiguration/js/pimcore/startup.js',
            '/bundles/mopeconfiguration/js/pimcore/settings/config/panel.js'
        ];
    }

    /**
     * @return array
     */
    public function getCssPaths()
    {
        return [
            '/bundles/mopeconfiguration/css/backend.css',
        ];
    }
}
